#include "sapi.h"
#include "arm_math.h"

q7_t q7_mul( q7_t a, q7_t b)
{
    q15_t ret;

    ret = a * b;
    ret = ret << 1;

    return ret >> 8;
}

uint16_t q7_print(char * buf, q7_t num)
{
    int i;
    float ans = (num & 0x80) ? -1 : 0;

    for(i = 1; i < 8; i++)
    {
        if(num & (0x80 >> i))
        {
            ans += 1.0 / (1U << i);
        }
    }

    return sprintf(buf, "0x%x | %f", num, ans);
}

void main(void)
{
    q7_t a = 0x040;
    q7_t b = 0x23;
    q7_t res = 0x0;
    char buf[100];
    uint16_t len = 0;

    boardConfig();
    uartConfig(UART_USB, 115200);

    res = q7_mul(a, b);
    len = q7_print(buf, res);

    uartWriteByteArray(UART_USB ,buf ,len);

    while(true){}
}