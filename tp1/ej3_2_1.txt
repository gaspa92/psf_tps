No es posible diferencia ambas señales debido al efecto de aliasing de la señal de mayor frecuencia. Dado que la señal tiene una frecuencia mayor a la frecuencia de muestreo dividida por 2, el alias de dicha señal tiene una frecuencia exactamente igual a la de la otra señal, por lo que ambas se superponen y no es posible distinguirlas.

Para solucionar este problema se puede utilizar un filtro anti alias que elimine las señales de mayor frecuencia o aumentar la frecuencia de muestreo.
