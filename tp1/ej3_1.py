import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

def get_sine(fs, f0, amp, samples, phase):
    if amp < 0: amp = 0
    elif amp > 1: amp = 1
    samples_array = np.arange(samples)
    return amp * np.sin(2 * np.pi * f0 * samples_array / fs + phase)

def get_square(fs, f0, amp, samples):
    return get_sine(fs, f0, amp, samples, 0) >= 0

def get_triangle(fs, f0, amp, samples):
    if amp < 0: amp = 0
    elif amp > 1: amp = 1

    samples_array = np.arange(samples)
    return amp * signal.sawtooth(2 * np.pi * f0 * samples_array / fs, 0.5)

    return ara

samples = 1000
fs = 1000
time = np.arange(samples) * 1/fs

# Generate sine wave
f0 = 1
amp = 1
phase = 0
sine_wave = get_sine(fs, f0, amp, samples, phase)

# Generate square wave
f0 = 10
amp = 1
square_wave = get_square(fs, f0, amp, samples)

# Generate triangle wave
f0 = 10
amp = 1
triangle_wave = get_triangle(fs, f0, amp, samples)

plt.plot(time, sine_wave, 'ro-')
plt.plot(time, square_wave, 'go-')
plt.plot(time, triangle_wave, 'bo-')
plt.show()
