#include "sapi.h"

struct header_struct {
   char     head[4];
   uint32_t id;
   uint16_t N;
   uint16_t fs ;
   char     tail[4];
} header={"head",0,256,5000,"tail"};

int main(void)
{
   uint16_t sample = 0;
   int16_t adc_value = 0;

   boardConfig();
   uartConfig(UART_USB, 460800);
   adcConfig(ADC_ENABLE);
   cyclesCounterInit(EDU_CIAA_NXP_CLOCK_SPEED);

   while(1)
   {
        cyclesCounterReset();

        adc_value = (int16_t)adcRead(CH1) - 512;
        uartWriteByteArray(UART_USB, (uint8_t*)&adc_value, sizeof(adc_value));

        if (++sample == header.N)
        {
            sample = 0;
            header.id++;
            uartWriteByteArray(UART_USB, (uint8_t*)&header, sizeof(header));
            adcRead(CH1); // Se hace una primera lectura para evitar errores
        }
        while(cyclesCounterRead() < (EDU_CIAA_NXP_CLOCK_SPEED / header.fs))
            ;
    }
}